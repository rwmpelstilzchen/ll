\subsection{Welsh}
\label{sec:welsh}

Welsh is a Brythonic Celtic language (Insular Celtic, Indo-European) spoken mainly in Wales by more than half a million speakers, virtually all of whom are bilingual with English (about a fifth of the population).
Along with other features, including syntactic and morphological ones \parencite{haspelmath.m:2001:sae}, its phonology differs greatly from that of other Western European languages in its proximity, having areally-unusual sounds such as voiceless nasals (\ipaem{m̥}, \ipaem{n̥} and \ipaem{ŋ̊}, spelt \grapheme{mh}, \grapheme{nh} and \grapheme{ngh}) and a voiceless alveolar lateral fricative (\ipaem{ɬ}, spelt \grapheme{ll}).

This lateral fricative originates from Proto-Brythonic (<~Proto-Celtic < Proto-Indo-European) \reconstructed{l-} (a voiced lateral approximant) and \reconstructed{sl-}%
\footnote{Through Brythonic \reconstructed{s-}~>~\reconstructed{h-}, thus \reconstructed{sl-}~>~\reconstructed{hl-}, regular development; see \cite[§~IX]{schrijver.p:1995:british-celtic}.}
in word-initial position and from non-lenited \reconstructed{L} and \reconstructed{LL}%
\footnote{In Celtic historical linguistics capital letters are used for indicating non-lenited consonants.}
in all positions \parencites[§§~91, 93, 127]{jackson.k:1953:language-history}[§~5.1]{schrijver.p:1995:british-celtic}.
In Late Brythonic \reconstructed{L} and \reconstructed{-LL-} stood side by side with lenited \reconstructed{l} (possibly having an optional voiceless \ipaet{l̥(ː)} realization already);
in Primitive Welsh the realization became obligatorily voiceless;
by the \nth{10} century \ipaem{ɬ} was fully established \parencite[§~93]{jackson.k:1953:language-history}.

Thus, \reconstructed{l} did not change indiscriminately into \ipaem{ɬ}: \reconstructed{l} in all positions which did not change into \ipaem{ɬ} remained \ipaem{l} in Welsh.
This ultimately resulted in a phonological \opposition{\ipaem{ɬ}}{\ipaem{l}} distinction, with minimal pairs such as \C{dal} \ipaem{-l} ‘to continue, to hold, to catch’~<~Proto-Brythonic \reconstructed{dalg-}~<~\reconstructed{del(ə)gʰ-} and \C{dall} \ipaem{-ɬ} ‘blind’~<~Celtic \reconstructed{\ipa{du̯allos}}~<~\reconstructed{\ipa{du̯l̥los}}%
\footnote{Here \grapheme{\ipa{l̥}} denotes a syllabic \reconstructed{\ipa{l}}, as it is customary in Indo-European historical linguistics, and not a voiceless alveolar lateral approximant \ipaet{l̥}.}%
~<~Proto-Indo-European \reconstructed{dʰ(e)wel-}.

The connection between \ipaem{l} and \ipaem{ɬ} in Welsh does not stop at diachrony; they are connected synchronically as well.
In order for us to discuss this synchronic connection, the notion of \emph{consonant mutation} must be introduced \parencites{hannahs.s.j:2011:celtic-mutations}{ball.m.j+:1992:mutation}.
It is a feature of all modern Celtic languages that, very broadly, refers to morphologized%
\footnote{The phenomenon started as purely allophonic and then went morphologization; \cite[see][§~4]{hickey.r:1996:typological-mutation}.}
phonological alternations in the initial consonant of a word (or of a component within compounds); see \textcites{zimmer.s:2005:mutations}{grijzenhout.j:2011:consonant-mutation} for typological discussion and \textcite{hickey.r:1996:typological-mutation} for a diachronic-typological study).
\Cref{tab:welsh mutations} gives an overview of the initial consonant mutations in Welsh.

\begin{table}
	\label{tab:welsh mutations}%
	\centering
	\begin{tabular}{cclcclcclcc}
		\toprule
		\multicolumn{2}{c}{Radical}  & & \multicolumn{2}{c}{Soft}    & & \multicolumn{2}{c}{Nasal}  & & \multicolumn{2}{c}{Aspirate} \\
		%& \multicolumn{2}{c}{\downbracefill}&& \multicolumn{2}{c}{\downbracefill}&& \multicolumn{2}{c}{\downbracefill}&& \multicolumn{2}{c}{\downbracefill}\\
		\midrule
		p   & \ipaem{p}   &  & b  & \ipaem{b} &  & mh  & \ipaem{m̥} &  & ph & \ipaem{f}\\
		t   & \ipaem{t}   &  & d  & \ipaem{d} &  & nh  & \ipaem{n̥} &  & th & \ipaem{θ}\\
		c   & \ipaem{k}   &  & g  & \ipaem{g} &  & ngh & \ipaem{ŋ̊} &  & ch & \ipaem{χ}\\[1.5ex]
		b   & \ipaem{b}   &  & f  & \ipaem{v} &  & m   & \ipaem{m}    \\
		d   & \ipaem{d}   &  & dd & \ipaem{ð} &  & n   & \ipaem{m}    \\
		g   & \ipaem{g}   &  & ∅  & \ipaem{}  &  & ng  & \ipaem{ŋ}    \\[1.5ex]
		\textbf{ll}  & \textbf{/}\boldll\textbf{/}   &  & \textbf{l}  & \textbf{/l/}\\
		rh  & \ipaem{r̥}   &  & r  & \ipaem{r}\\
		m   & \ipaem{m}   &  & f  & \ipaem{v}\\
		(ts & \ipaem{t͡ʃ}) &  & (j & \ipaem{d͡ʒ})\\
		\bottomrule
	\end{tabular}\vspace{0.5\baselineskip} % Ugly workaround!
	\caption{%
		An overview of the consonant mutations in Welsh.
	}
\end{table}

In order to clarify by example (on the right),%
\marginnote{
	\begin{tabular}{lll}
		\C{tad}     & \ipaem{taːd}   & father\\
		\C{ei thad} & \ipaem{i θaːd} & her father\\
		\C{ei dad}  & \ipaem{i daːd} & his father\\
		\C{eu tad}  & \ipaem{i taːd} & their father
	\end{tabular}
}
let us consider the third person possessive pronouns \parencite[§~4.2]{hickey.r:1996:typological-mutation};
\C{ei} \ipaem{i} followed by an aspirate mutation indicates a \grammar{3.sg.f} possessor,
\C{ei} \ipaem{i} followed by a soft mutation (also called ‘lenition’) indicates a \grammar{3.sg.m} possessor and
\C{eu} \ipaem{i} followed by the radical (no mutation) indicates \grammar{3.pl} possessors (\grapheme{ei} and \grapheme{eu} are homonyms, differing only in orthography).

Now, the relevant point is that synchronically \ipaem{l} is the soft-mutated (‘lenited’) form of \ipaem{ɬ}.
This can be demonstrated by the third person possessive pronouns as before (on the right;%
\marginnote{
	\begin{tabular}{lll}
		\C{llygaid}    & \ipaem{ˈɬəgaɨd}     & eyes\\
		\C{ei llygaid} & \ipaem{i ˈɬəgaɨd}   & her eyes\\
		\C{ei lygaid}  & \ipaem{i ˈləgaɨd} & his eyes\\
		\C{eu llygaid} & \ipaem{i ˈɬəgaɨd}   & their eyes
	\end{tabular}
}
empty cells in \cref{tab:welsh mutations} imply no mutation).
From a historical linguistic point of view, this can be seen as a continuation of the \opposition{\reconstructed{l}}{\reconstructed{L}} opposition referred to above, but in synchronic terms the Welsh mutation system ties \ipaem{ɬ} and \ipaem{l} together in morphology.
In a sense, the \ipaem{ɬ}→\ipaem{l} soft mutation is comparable with the voicing in the \ipaem{p}→\ipaem{b}, \ipaem{t}→\ipaem{d}, \ipaem{k}→\ipaem{g} and \ipaem{r̥}→\ipaem{r} soft mutations, but \ipaem{ɬ} and \ipaem{l} differ not only in voicing but also in manner of articulation (a fricative obstruent and an approximant, respectively); see \textcite{ball.m.j:1990:lateral-fricative} concerning analysis. 
Regarding the diachronic devoicing of Brythonic *\ipaem{l} into Welsh \ipaem{ɬ}, not stopping at the intermediate \ipaet{l̥}, I offer the explanation that this change might be linked to the phonetic properties of \ipaet{l̥} (in particular the measurable voicing in the later stage of its articulation; \cite[see][]{maddieson.i+:1984:voiceless-lateral}): although in naïve phonological terms it is \ipaet{l̥} that is the voiceless counterpart of \ipaem{l}, in actuality it is \ipaem{ɬ} that can play this role better, as it is more prototypically voiceless.

To the best of my knowledge, none of the few%
\footnote{\textcite[32]{czerniak.t:2015:clusters-welsh} counts five previous publications: \textcite[§~3.1.2]{hannahs.s.j:2013:phonology-welsh}, \textcite{iosad.p:2012:substance-free}, \textcite{awbery.g.m:1984:phonotactic-welsh} and two later books by Awbery (1986 and 2010).}
publications written on phonotactic constraints in Welsh deal with the specificities of the phonotactics of \ipaem{ɬ}.
In order to begin to fill this gap I ran queries on a word-list of total length of 22923 entries, extracted from a digital dictionary \parencite{nodine.m:2003:welsh-english} in order to check where \foreign{ll} \ipaem{ɬ} occurs.%
\footnote{%
	These quick queries can confirm certain combinations occur in the written language, but they cannot reject the possibility of combinations that maybe are absent from the word-list but are present in actual use.
	Nevertheless, the word-list is long and varied enough to give a relatively high degree of certainty.
	Querying a dictionary proved more effective for this purpose than querying a lexical database such as CEG \parencite{ellis.n.c+:2001:ceg}.
}
Spoken and written languages are of course different, but the following results can give an approximation to the status of \ipaem{ɬ} in spoken Welsh, in the absence of direct phonological data based on the spoken language.

\begin{compactitem}
	\item No initial \foreign{llC-} or medial \foreign{-CllC-} occur.
	%read !grep "ll[^AEIOUWYaeiouwyâêîôŵŷû]\+$" /tmp/b.txt -o | sort | uniq -c | sort -nr
	\item Word-final \foreign{-llC} is limited to numerous occurrences of \foreign{-llt} \parencite[43]{jones.g.e:1984:distinctive-welsh} and one loan-word containing \foreign{-lltr} (\foreign{cwlltr} ‘coulter’, from Latin \foreign{cultrum}, which have an alternative spelling with no \foreign{-lltr}, in which an epenthetic copy vowel is inserted%
		\footnote{\cite[See][§5.1]{hannahs.s.j:2013:phonology-welsh} for a discussion of this phenomenon.}%
		: \foreign{cwlltwr}).
	%read !grep "[^AEIOUWYaeiouwyâêîôŵŷû]\+ll$" /tmp/b.txt -o | sort | uniq -c | sort -nr
	\item Word-final \foreign{-Cll} is limited to one loan-word and its compound derivatives: \foreign{iarll} ‘earl’, from Old Norse \foreign{jarl} ‘earl; (\emph{poet.}) a highborn, noble man or warrior’.
	%read !grep "[AEIOUWYaeiouwyâêîôŵŷû][^AEIOUWYaeiouwyâêîôŵŷû]\+ll[AEIOUWYaeiouwyâêîôŵŷû]" /tmp/b.txt -o |sed 's/[AEIOUWYaeiouwyâêîôŵŷû]//g' | sort | uniq -c | sort -nr
	\item Word-medial \foreign{-CllV-} is virtually limited to cases of compound or suffix boundaries between the \foreign{C} and \foreign{ll}: mostly \foreign{-nll-} and \foreign{-rll-}, with a few cases of \foreign{-mll-}, \foreign{-cll-}, \foreign{-frll-}, \foreign{-rnll-}, \foreign{-gll-}, \foreign{-fnll-}, \foreign{-sll-} and \foreign{-rmll-}.
	%read !grep "[AEIOUWYaeiouwyâêîôŵŷû]ll[^AEIOUWYaeiouwyâêîôŵŷû]\+[AEIOUWYaeiouwyâêîôŵŷû]" /tmp/b.txt -o |sed 's/[AEIOUWYaeiouwyâêîôŵŷû]//g' | sort | uniq -c | sort -nr
	\item Word-medial \foreign{-VllC-} seems to have similar limitations with regards to morphological boundaries (excluding cases of elements ending with \foreign{-llt} followed by an element beginning with a vowel).
		By order of number of occurrences, these combinations are attested: \foreign{-llt-}, \foreign{-llg-}, \foreign{-llf-}, \foreign{-lltr-}, \foreign{-lln-}, \foreign{-llh-}, \foreign{-llbr-}, \foreign{-llr-}, \foreign{-llbl-}, \foreign{-llm-}, \foreign{-lltn-}, \foreign{-lltg-}, \foreign{-llff-}, \foreign{-llddr-}, \foreign{-lldd-}, \foreign{-lld-}, \foreign{-lltl-}, \foreign{-lltgl-}, \foreign{-lltf-}, \foreign{-lltb-}, \foreign{-ll-l-}%
		\footnote{%
			In \foreign{all-lein} ‘off-line’, a neologism compounding native \foreign{all-} ‘extra-, ex-, off-’ and borrowed \foreign{lein} ‘line’.
		},
		\foreign{-llfr-}.
	\item \foreign{-ll(-)} occurs after all vowels but not after all diphthongs (the more conservative North Wales forms are given here in IPA transcription):
		\begin{compactitem}
			\item it occurs within the boundaries of one morpheme after the \ipaem{-ɨ̯} diphthongs \foreign{ae} \ipaem{ɑːɨ̯} and \foreign{wy} \ipaem{ʊ̯ɨ, u̯ɨ} and the \ipaem{-i̯} diphthongs \foreign{ai} \ipaem{ai̯} and \foreign{ei} \ipaem{əi̯};
			\item it occurs in a separate morpheme (the adjectival suffix \foreign{-llyd, -lyd}) after \foreign{ew} \ipaem{ɛu̯, eːu̯} in \foreign{rhewllyd} ‘icy’ (from \foreign{rhew} ‘ice’) and \foreign{drewllyd} ‘stinking’ (from \foreign{drewi} ‘to stink’), both have \foreign{-ewlyd-} variants \foreign{rhewlyd} and \foreign{drewlyd};
			\item in the said dictionary there were no occurrences of \foreign{ll} after the \ipaem{-ɨ̯} diphthongs \foreign{au} \ipaem{aɨ̯}, \foreign{eu} \ipaem{əɨ̯}, \foreign{ey} \ipaem{əɨ̯}, \foreign{oe} \ipaem{ɔɨ̯, ɔːɨ̯}%
				\footnote{\foreign{troell} ‘spinning-wheel’ (from \foreign{tro} ‘rotation’ or \foreign{troi} ‘to turn, to spin’~+ diminutive suffix \foreign{-ell}) has no diphthong but \foreign{o+e}. Orthographically it can be written as \foreign{tröell}.}
				and \foreign{ou} \ipaem{ɔɨ̯, ɔːɨ̯},
				the \ipaem{-i̯} dipthong \foreign{oi} \ipaem{ɔi̯},
				or after the \ipaem{-u̯} diphthongs \foreign{aw} \ipaem{au̯, ɑːu̯}, \foreign{iw} \ipaem{ɪu̯}, \foreign{ow} \ipaem{ɔu̯}, \foreign{uw} \ipaem{ɨu̯} and \foreign{yw} \ipaem{ɨu̯, əu̯}.
				Therefore, it does not occur after \ipaem{-u̯} within morphological boundaries.
		\end{compactitem}
\end{compactitem}

Summarizing the results, \foreign{ll} is limited mainly to the following positions: word-initial followed by a vowel; word-final after a vowel; intervocal; word-medial with a preceding or following consonant across morpheme boundaries or \foreign{-llt+V-}; word-final \foreign{-llt} (as well as \foreign{-lltr} and \foreign{-rll} in a few loan-words); following certain diphthongs.

The second element in compounds is usually lenited, making the soft mutation a \foreign{Fügemorphem} (binding morpheme; see \textcite[§~4.a.4]{shisha-halevy.a:2003:juncture-welsh}).
This has obvious phonotactic implications on \ipaem{ɬ}, as it is usually softened to \ipaem{l} in compounds.
