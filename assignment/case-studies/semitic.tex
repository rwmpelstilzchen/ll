\subsection{Semitic}
\label{sec:semitic}

While the previous subsection discussed scenarios in which sibilant fricatives develop into lateral ones, this section demonstrates the opposite direction.

Two%
\footnote{Another view reconstructs an additional voiced lateral fricative, \reconstructed{ź} (\reconstructed{ẑ}) \parencites{voigt.r:1992:lateralreihe}[71]{goldenberg.g:2012:semitic}.}
lateral fricatives are reconstructed in Proto-Semitic, usually designated as \grapheme{\reconstructed{ś}} and \grapheme{\reconstructed{ṣ́}} (or \grapheme{\reconstructed{ŝ}} and \grapheme{\reconstructed{ṣ̂}}) in the Semitist literature (a dot diacritic marks a phoneme as ‘emphatic’; \cite[see][§~1.3.1]{kogan.l:2001:semitic-phon}).
In all contemporary Semitic languages apart from Modern South Arabian languages, and in some ancient languages as well, these phonemes did not remain lateral: in most cases they merged with existing sibilant phonemes.
See \cref{tab:semitic-correspondences} for regular correspondences.

\newcommand{\abbrv}[2]{\emph{#1} #2}
\begin{table*}
	\label{tab:semitic-correspondences}%
	%\caption[][-10.5\baselineskip]{% % Ugly!!!
	\caption[][1\baselineskip]{% % Ugly!!!
		Regular correspondences of the Proto-Semitic consonants; reproduced from the relevant rows in table~6.2 in \textcite[§~1.2]{kogan.l:2001:semitic-phon}, substituting circumflex notation (\grapheme{\reconstructed{ŝ}}/\grapheme{\reconstructed{ṣ̂}}) with the more common acute accent (\grapheme{\reconstructed{ś}}/\grapheme{\reconstructed{ṣ́}}) notation.
		%Abbreviations:
		%\abbrv{PS}{Proto-Semitic},
		%\abbrv{Akk.}{Akkadian},
		%\abbrv{Ugr.}{Ugaritic},
		%\abbrv{Hbr.}{(Biblical) Hebrew},
		%\abbrv{Syr.}{Syriac},
		%\abbrv{Arb.}{Arabic},
		%\abbrv{Sab.}{Sabaic},
		%\abbrv{Gez.}{Gəʿəz},
		%\abbrv{Tgr.}{Tigre},
		%\abbrv{Tna.}{Tigrinya},
		%\abbrv{Amh.}{Amharic},
		%\abbrv{Har.}{Harari},
		%\abbrv{Gur.}{Gurage},
		%\abbrv{Mhr.}{Mehri},
		%\abbrv{Jib.}{Ǧībbali},
		%\abbrv{Soq.}{Soqotri}.
	}%
	\begin{tabular}{lllllllllllllll}
		\toprule
		PS &
		Akk. &
		Ugr. &
		Hbr. &
		Syr. &
		Arb. &
		Sab. &
		Gez. &
		Tgr., Tna. &
		Amh. &
		Har. &
		Gur. &
		Mhr. &
		Jib. &
		Soq.
		\\
		\midrule
		{*s} &
		s &
		s &
		s &
		s &
		s &
		s₃ &
		s &
		s, š &
		s, š &
		s, š &
		s, š &
		s &
		s &
		s
		\\
		{*š} &
		š &
		š &
		š &
		š &
		s &
		s₁ &
		s &
		s, š &
		s, š &
		s, š &
		s, š &
		š, h &
		š, s &
		š, h
		\\
		{*ṣ} &
		ṣ &
		ṣ &
		ṣ &
		ṣ &
		ṣ &
		ṣ &
		ṣ &
		ṣ, č̣ &
		ṭ, č̣ &
		ṭ, č̣ &
		ṭ, č̣ &
		ṣ, ṣ̌ &
		ṣ &
		ṣ
		\\
		{*ś} &
		š &
		š &
		ś &
		s &
		š &
		s₂ &
		ś &
		s, š &
		s, š &
		s, š &
		s, š &
		ś &
		ś &
		ś
		\\
		{*ṣ́} &
		ṣ &
		ṣ &
		ṣ &
		ʿ &
		ḍ &
		ṣ́ &
		ṣ́ &
		ṣ, č̣ &
		ṭ, č̣ &
		ṭ, č̣ &
		ṭ, č̣ &
		ź &
		ẓ́ &
		ẓ́
		\\
		\bottomrule
	\end{tabular}
\end{table*}

The reconstruction of lateral fricatives in Proto-Semitic goes back to Richard Lepsius in 1861, but it was \textcite{steiner.r:1977:laterals-semitic}, later followed by \textcite{steiner.r:1991:addenda-laterals-semitic}, that was decisive for the wide recognition of the hypothesis.
\textcite{steiner.r:1977:laterals-semitic} is structured by lines of evidence, from diverse directions (see \textcite[§1.3.3]{kogan.l:2001:semitic-phon} for a short overview).
I will mention some of these lines of evidence here, because of their general benefit for understanding lateral fricatives.

The most direct evidence is the fact that Modern South Arabian languages preserve lateral fricative reflexes for \reconstructed{ś} and \reconstructed{ṣ́} (\ipaem{ɬ} and \ipaem{ɬˤ{\textasciitilde}ɬʼ} respectively); see \textcites[ch.~2f.]{steiner.r:1977:laterals-semitic}[§~1.3.3.1]{kogan.l:2001:semitic-phon}.%
\footnote{%
	Note that ‘Arabian’, in the geographical sense, is to distinguished from ‘Arabic’; these languages are spoken in the south of the Arabian peninsula.

	\vspace{0.25\baselineskip}
	\noindent
	{\color{gray}\fbox{\normalcolor\includegraphics[width=0.75\linewidth]{msal.pdf}}}

	\noindent
	Source: \href{https://commons.wikimedia.org/wiki/File:Modern_South_Arabian_Languages.svg}{Wikimedia Commons}.
}
Modern South Arabian languages are represented in \cref{tab:semitic-correspondences} by Meheri \emph{(Mhr.)}, Ǧībbali \emph{(Jib.)} and Soqotri \emph{(Soq.)} in the rightmost columns; they are descendant from Ancient South Arabian languages, represented in the table by Sabaic (abbreviated as \emph{Sab.}).
Evidence from a marginal, however conservative, group of languages cannot suffice.
Luckily, ancient Semitic languages provide ample evidence.

While there are obviously no recordings from earlier times, early grammarians can provide invaluable information.
Sībawayhi (c.~760–796) is one of the most important grammarians in the native Arabic grammatical tradition.
Concerning the \foreign{muḫraǧ} (\Arabic{مخرج} ‘place of articulation’) of \Arabic{ض} (\foreign{ḍād}, which is pronounced \ipaet{dˤ} in most contemporary Arabic dialects) he writes in his \foreign{Kitāb}:
\begin{quote}
	\foreign{min bayni ʾawwali ḥāffati l-lisāni wa-mā yalīhi mina l-ʾaḍrās}

	‘between the beginning of the tongue’s edge and the corresponding molars’
\end{quote}
This, when compared with the similar \foreign{muḫraǧ} of \Arabic{ل} \foreign{(lām)}, which is beyond doubt an \ipaem{l} phoneme (voiced lateral approximant), implies a lateral articulation of \foreign{ḍ} (<~Proto-Semitic \reconstructed{ṣ́}), presumably \ipaet{ɮˤ} or \ipaet{d͡ɮˤ} (see \textcite[ch.~4]{steiner.r:1977:laterals-semitic} for a detailed discussion; see also \textcite[§~1.3.3.2]{kogan.l:2001:semitic-phon}).

In addition to the descriptive evidence from Sībawayhi’s times and dialect the fact that Arabic came in contact with numerous languages which borrowed words from it can provide further evidence for the lateral articulation of \foreign{ḍ} \parencites[see][ch.~5–8]{steiner.r:1977:laterals-semitic}[§~1.3.3.4]{kogan.l:2001:semitic-phon}{versteegh.k:1999:loanwords-merger}.
\Cref{tab:arabic-loanwords} demonstrates some lateral phonemes or phoneme sequences in words which were borrowed from Arabic words containing \foreign{ḍ}.

\begin{table}
	\label{tab:arabic-loanwords}%
	\caption{Loanwords from Arabic words containing \foreign{ḍ}.}%
	\begin{tabular}{lllll}
		\toprule
		language & loanword          & gloss          &   & Arabic form\\
		\midrule
		Spanish  & \foreign{alca\textbf{ld}e} & ‘judge, mayor’ & < & \foreign{ʾal-qā\textbf{ḍ}(ī)}\\
		Malay    & \foreign{\textbf{dl}oha}   & ‘morning’      & < & \foreign{\textbf{ḍ}uḥā}\\
		Hausa    & \foreign{hai\textbf{l}a̱}   & ‘menstruation’ & < & \foreign{ḥay\textbf{ḍ}}\\
		Somali   & \foreign{ár\textbf{l}i}    & ‘country’      & < & \foreign{ʾar\textbf{ḍ}}\\
		\bottomrule
	\end{tabular}
\end{table}

Evidence from loan words is not limited to Arabic \foreign{ḍ}.
\Greek{βάλσαμον} \foreign{(bálsamon)} is the Greek name of the plant \foreign{Commiphora gileadensis},%
\marginnote{%
	%\begin{minipage}[t]{2cm}
		%\vspace{0pt}
		\includegraphics[width=3cm]{bálsamon.jpg}
	%\end{minipage}\quad

	%\begin{minipage}[t]{2.65cm}
		%\vspace{0pt}
		\noindent
		\foreign{Commiphora gileadensis}, an illustration by Petronella J.M. Pas (1881); from \href{https://commons.wikimedia.org/wiki/File:Balsamodendron_ehrenbergianum00.jpg}{Wikimedia Commons}.
	%\end{minipage}
}
treasured in the ancient world for its sap which was used in perfumes and medicine.
The Greek word is of a certain Semitic origin \parencite[\Greek{βάλσαμον}, p.~217]{frisk.h:1960:griechisches-worterbuch-1}; see \textcite[ch.~16]{steiner.r:1977:laterals-semitic} for a detail discussion about the path of borrowing and \textcite[§~1.3.3.18]{kogan.l:2001:semitic-phon} for a summary.
The donor word was Hebrew \Hebrew{בֹּשֶׂם}%
\footnote{%
	Concerning the orthographic double duty (polyphony) of the Hebrew \grapheme{\Hebrew{ש}} \foreign{(šīn)}, which was used for both \foreign{š} \ipaem{ʃ} and \foreign{ś} \ipaem{ɬ}, see \textcites[§~2.1]{rendsburg.g.a:2013-:phonology-biblical}[§~7.3]{goldenberg.g:2012:semitic}.
	Such polyphony is not unique to \grapheme{\Hebrew{ש}}: \grapheme{\Hebrew{ח}} \foreign{(ḥēṯ)} was used for both \foreign{ḫ} and \foreign{ḥ}, and \grapheme{\Hebrew{ע}} \foreign{(ʿayin)} was used for both \foreign{ǵ} and \foreign{ʿ} \parencite[§~2.2]{rendsburg.g.a:2013-:phonology-biblical}.
}
\foreign{bōśɛm} or \Hebrew{בָּשָׂם} \foreign{bāśām}, or a cognate from another Semitic donor (Phoenician? South Arabian?).
Here, in a similar manner to Spanish \foreign{alcalde} and Malay \foreign{dloha}%
\footnote{
	Malay \foreign{dl} may reflect Arabic \ipaet{d͡ɮˤ}, if that was indeed the realization in the Arabic variety that influenced Malay.
},
a lateral phoneme is split into two phonemes (see \cref{sec:further-research}).
Similar evidence can be observed from the Hebrew ethnonym \Hebrew{כַּשְׂדִּים} \foreign{kaśdīm} ‘Chaldaeans’ in comparison to Akkadian \foreign{kaldu} or \foreign{kaldāy-} \foreign{(kal-da-a-a)} (> Aramaic \emph{kaldāy}~> Septuagint Greek \Greek{χαλδαίοι} \foreign{kʰaldaíoi}; see \textcites[ch.~18]{steiner.r:1977:laterals-semitic}[§~1.3.3.20]{kogan.l:2001:semitic-phon}) and from the epigraphic Gəʿəz place name \Geez{መፀ} \foreign{m(ä)ṣ́(ä)}, rendered in Greek as \Greek{μάτλια} \foreign{mátlia} \parencites{rodinson.m:1981:axoum-bedjas}{weninger.s:1998:realisation-altathiopischen}.

Another line of evidence is the phonotactic incompatibility of the Semitic lateral fricatives with the lateral approximant \reconstructed{l} \parencites[see][ch.~13]{steiner.r:1977:laterals-semitic}[1504–1506]{steiner.r:1991:addenda-laterals-semitic}[§~1.3.3.6]{kogan.l:2001:semitic-phon}.

The evidence discussed here (direct evidence from living Semitic languages, the writings of early grammarians, loan-words, rendition of names in Greek and phonotactics) can shed light on lateral fricatives in language history, grammar and language contact.

As stated above, all contemporary Semitic languages apart from Modern South Arabian languages lost their lateral fricative phonemes: in most cases they merged into sibilant phonemes, in Aramaic/Syrian into \foreign{ʿ} \ipaem{ʕ} and in Arabic it changed to \foreign{ḍ} (\ipaet{dˤ} in most dialects).
It is that probable language contact is a contributing factor in this widespread change, which took place over a long period (from at least ancient Akkadian in one end to late antiquity Arabian in the other); the remote and relatively isolated South Arabians being an exception.
Notice that no Semitic language demonstrates a *\ipaem{ɬ(ʼ)}~>~\ipaem{l} change: this can be due to the said language contact and/or can indicate some ‘hissing’ (sibilant) quality in the articulation of the original Semitic lateral fricatives, which resulted in full assibilation.

The Modern South Arabian languages show that lateral fricatives \emph{can} be very stable: without getting into the vexed question of dating Proto-Semitic, stability and continuity are demonstrated here over a great deal of time.%
\footnote{One could argue these two or three lateral phonemes go back to an even earlier, Proto-Afro-Asiatic stage \parencites[see, for example,][]{orel.v.e+:1995:hamito-semitic}{ehret.ch:1995:proto-afroasiatic}[§8]{bomhard.a.r:2008:afrasian-phonology}, but it is quite problematic from an epistemological point of view: over such long periods of time the comparative method cannot work as well and many of the reconstructions are not scientifically valid; \cite[see][p.~159f.]{steiner.r:1977:laterals-semitic}.}
Evidence for the degree of influence of language contact on the stability of lateral fricatives can be obtained from another case.
\textcite{ball.m.j+:2001:acquisition-ll} conducted an experimental study of the acquisition of Welsh phonology in bilingual Welsh-English children.
They found clear differences in the acquisition of \ipaem{ɬ} between Welsh-dominant and English-dominant subjects: in all age groups the English-dominant produced \ipaem{ɬ} less accurately.
Only in the last age group (Group~E, age range 4;6–5;0) the Welsh-dominant subjects achieved 100\% accuracy, while the English-dominant ones achieved 81\%, 67\% and 50\% for initial, medial and final positions respectively in that age group.
As imperfect acquisition can impede intergenerational retention and stability, this study can shed light on the retention of the Semitic lateral fricatives (as well as other linguistic features) in South Arabian languages, whose speakers live on the edge of the Semitic world, bordered by deserts and the ocean (or water from all sides in the case of the island dwelling Soqotri people).
