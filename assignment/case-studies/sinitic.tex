\subsection{Sinitic}
\label{sec:sinitic}

The genealogy of Sinitic languages (or ‘dialects’) is far from a simple one.
Continuous language contact between them and the phonetic opacity of the Chinese logographic writing system make it difficult to draw definite conclusions concerning the relationship between these languages and to reconstruct proto-languages.
This subsection deals mainly with two Sinitic languages in which older sibilants developed into voiceless lateral fricatives: Táishān (a Yuè language) and Púxiān (a Mǐn language); see \cref{fig:sinitic-ɬ} for a map with these languages marked within the greater context of Chinese language groups, and map~7.2 in \textcite[167]{de-sousa.h:2015:nanning} for some Sinitic languages with lateral fricatives, including these two.

%Their common ancestor did not share this development, many other Yuè and Mǐn languages have sibilant reflexes for the older sibilant phonemes and so are the languages spoken in the geographical area between them.

\begin{figure}
	\label{fig:sinitic-ɬ}%
	\includegraphics[width=\linewidth]{sinitic-ɬ.pdf}%
	\caption{%
		A general map of Sinitic language groups (from \href{{https://commons.wikimedia.org/wiki/File:Map_of_sinitic_languages_full-en.svg}}{Wikimedia Commons}, based on \cite{wurm.s.a+:1987:atlas-china-1}).
		The two languages under discussion are marked with red circles.
	}
	%\setfloatalignment{b}
\end{figure}

According to \textcite[p.~299f.]{cheng.t.m:1973:phonology-taishan} the development of Táishān exhibits a push-chain phenomenon from Middle Chinese%
\footnote{Most Chinese languages, including Yuè, can be traced back to a language stage called Middle Chinese, recorded in the 601~CE rhyme dictionary \foreign{Qièyùn}.},
pushing the \ipaem{s} and \ipaem{z} fricatives of the dental series (‘Dent.~II’) out of the boundaries of the existing phonological inventory and into a new territory: a voiceless lateral fricative \ipaem{ɬ}.
In \posscite[187]{maddieson.i+:1984:voiceless-lateral} study this phoneme in Táishān was realized as a voiceless dental lateral fricative \ipaet{ɬ̪}, varied with \ipaet{θ} (a voiceless dental non-sibilant fricative) in the speech of several of their speakers.

According to \textcite[166–168]{de-sousa.h:2015:nanning} having \ipaem{ɬ} or \ipaem{θ} (depending on accent and language) is an areal phenomenon.
He discusses the possible influence of Kra-Dai languages (non-Sinitic) in the area and points out these sounds are found in languages in several non-contiguous areas (see map~7.2 in \cite[167]{de-sousa.h:2015:nanning}).
One of these languages is Púxiān.

Púxiān exhibits lateral reflexes for a wider scale of sibilants.%
\footnote{See \textcite{liu.f:2007:puxian-ll} for dialectological background.}
Judging from the transliterated texts in \textcite{nakajima.m:1979:min} and comparing them with Táishān forms from \textcite{stephen.l:taishanese} and the Middle Chinese forms they developed from I compiled \cref{tab:sinitic-comparison}, adding Mandarin and Standard Cantonese forms for completeness (Mandarin being the most common variety of all Chinese groups and Standard Cantonese being the most common Yuè variety).
The last three rows show how Middle Chinese \ipaem{ʃˠ-}, \ipaem{ʑ} and \ipaem{d͡z} did not developed into \ipaem{ɬ} in Táishān, yet in Púxiān their analogues did.
It should be noted that Púxiān, being a Mǐn dialect, is not simply a direct descendent that can be traced back to Middle Chinese, as Mǐn dialects preserve archaisms from before Middle Chinese \parencite[§~9.4]{norman.j:1988:chinese}.

\begin{table*}
	\label{tab:sinitic-comparison}%
	%\caption[][-18.5\baselineskip]{% % Ugly!!!
	\caption[][1\baselineskip]{% % Ugly!!!
		A comparison of reflexes for sibilant phonemes in Táishān and Púxiān, with Mandarin and Standard Cantonese (Standard Yuè) for completeness, showing all sibilant reflexes.
		Mandarin is given in \foreign{pīnyīn} translation and Cantonese in \foreign{jyut⁶ping³}, both followed by IPA transcription.
	}%
	\small
	\colorlet{mca}{\tablecolour{sronpaleblue}}
	\colorlet{mcb}{\tablecolour{sronpalegreen}}
	\colorlet{ta}{\tablecolour{sronpaleblue}}
	\colorlet{tb}{\tablecolour{sronpalegreen}}
	\colorlet{pa}{\tablecolour{sronpaleblue}}
	\renewcommand{\tabruby}[2]{#1 & #2}
	\begin{tabular}{cllllllll}
		\toprule
						   & meaning                                         &
		%P.S.T                                                                      &
		\multicolumn{1}{l}{Middle Chinese}                                                   &
		\multicolumn{1}{l}{Táishān}                                                      &
		\multicolumn{1}{l}{Púxiān}&
		\multicolumn{2}{l}{Mandarin}                                 &
		\multicolumn{2}{l}{Standard Cantonese}
		\\
		\midrule
		\tabruby{\Chinese{三}}{three}                                &
		%\foreign{*g-sum}                                                           &
		{\cellcolor{mca}}\ipaem{sam}                                 &
		{\cellcolor{ta}}\ipaem{ɬam³³}                                &
		{\cellcolor{pa}}\ipaem{ɬo³³}&
		\foreign{sān}                                                & \ipaem{sa̠n⁵⁵}             &
		\foreign{saam¹} & \ipaem{saːm⁵⁵}
		\\
		\tabruby{\Chinese{四}}{four}                                 &
		%\foreign{*b-ləj}                                                           &
		{\cellcolor{mca}}\ipaem{siɪ\textsuperscript{H}}              &
		{\cellcolor{ta}}\ipaem{ɬi³³}                                 &
		{\cellcolor{pa}}\ipaem{ɬi⁵²}&
		\foreign{sì}                                                 & \ipaem{sz̩⁵¹}             &
		\foreign{sei³, si³} & \ipaem{sei̯³³, siː³³}
		\\
		%\tabruby{\Chinese{死}}{die}                     &
		%%\foreign{*səj}                                                             &
		%{\cellcolor{mca}}\ipaem{sˠiɪ\textsuperscript{X}}              &
		%\foreign{sǐ}                                 &
		%\ipaem{sɨ˨˩˦}                                &
		%{\cellcolor{ta}}\ipaem{ɬi³³}                                 &
		%~
		%\\
		\tabruby{\Chinese{新}}{new}                                  &
		%\foreign{*siŋ}                                                             &
		{\cellcolor{mca}}\ipaem{siɪn}                                &
		{\cellcolor{ta}}\ipaem{ɬin³³}                                &
		{\cellcolor{pa}}\ipaem{ɬiŋ}&
		\foreign{xīn}                                                & \ipaem{ɕin⁵⁵}                                                &
		\foreign{san¹} & \ipaem{sɐn⁵⁵}
		\\
		\tabruby{\Chinese{生}}{\Stack{live\\be born}}                        &
		%\Stack{\foreign{*s-riŋ \textasciitilde}\\\foreign{s-r(j)aŋ}}               &
		{\cellcolor{mcb}}\ipaem{ʃˠæŋ\textsuperscript{(H)}}           &
		{\cellcolor{tb}}\ipaem{saŋ³³}                                &
		{\cellcolor{pa}}\begin{tabular}[t]{@{}ll@{}}\ipaem{ɬã, tsʰã} & (coll.) \\\ipaem{ɬɛŋ⁵⁵} & (lit.)\end{tabular}&
		\foreign{shēng}                                              & \ipaem{ʂɤŋ⁵⁵}                                                &
		\Stack{\foreign{saang¹}\\\foreign{sang¹}} & \begin{tabular}[t]{@{}ll@{}}\ipaem{saːŋ⁵⁵} & (coll.)\\\ipaem{sɐŋ⁵⁵} & (lit.)\end{tabular}
		\\
		\tabruby{\Chinese{食}}{eat, …}                               &
		%\foreign{*m/s/g-ljak}                                                             &
		{\cellcolor{mcb}}\ipaem{ʑɨk̚}                                 &
		{\cellcolor{tb}}\ipaem{sɛt³²}                                &
		{\cellcolor{pa}}\ipaem{ɬie²¹}&
		\foreign{(shí)}                                              & (\ipaem{ʂʐ̩³⁵})                                               &
		\foreign{sik⁶} & \ipaem{sɪk̚²}
		\\
		% \\
		% \tabruby{\Chinese{十}}{ten}                                                   &
		% %\foreign{*gip}                                                             &
		% \ipaem{d͡ʑiɪp̚}                            &
		% \foreign{shí} & \ipaem{sɨ˧˥}                                                 &
		% \ipaem{sip³²} &
		\tabruby{\Chinese{坐}}{sit}                                  &
		%
		{\cellcolor{mcb}}\ipaem{d͡zuɑ\textsuperscript{X}}             &
		{\cellcolor{tb}}\ipaem{tɔ³³}                                 &
		{\cellcolor{pa}}\ipaem{ɬœy⁵²}                                &
		\foreign{zuò}                                                & \ipaem{t͡su̯ɔ⁵¹}                                              &
		\Stack{\foreign{co⁵}\\\foreign{zo⁶}} & \begin{tabular}[t]{@{}ll@{}}\ipaem{t͡sʰɔː¹³} & (coll.)\\\ipaem{t͡sɔː²²} & (lit.)\end{tabular}
		\\
		\bottomrule
	\end{tabular}
\end{table*}

\textcite{chen.h:2018:assimilation-xianyou} discusses the Xiānyóu dialect of Púxiān. 
Table~1%
\footnote{%
	For a very similar table without reference to a specific dialect of Púxiān see table~1 in \textcite[§~1.4.1]{wu.j:2010:pronominal-puxian}.
	Both tables lack sibilant fricatives.
}
(‘Initial consonants (occurring in C₁ slot)’)%
\footnote{%
	Sinitic languages as a whole show a very limited consonantal paradigm in syllable-final position (C₂).
	Thus, the relevant position for the current discussion is syllable-initial.
}
in §~2.1.1 there shows no sibilant consonants save the affricates \foreign{ts-} and \foreign{tsʰ-}, as sibilant fricatives merged into a lateral \ipaem{ɬ} phoneme.
The lack of an \ipaem{s} phoneme of some kind, in particular in a language that \emph{has} other fricatives, is rather rare crosslinguistically \parencites[§~7.6.2]{lass.r:1984:phonology}[§~3.1]{maddieson.i:1984:sounds}.
%as pointed out in \textcite[§~2.1.1, p.~4]{chen.h:2018:assimilation-xianyou}

Of special interest for our discussion~— especially in connection with the previous subsection dealing with Welsh~— is \textcite[§§~3–4]{chen.h:2018:assimilation-xianyou}, which deals with a morphophonological phenomenon he calls \emph{initial assimilation} (after \Chinese{声母類化} \foreign{shēngmǔ lèihuà}, a term coined by \textcite{tao.y:1930:minyin-yanjiu}) in Xiānyóu.
As \textcite[n.~2 on p.~2]{yang.ch-y.h:2015:mutations-fuzhou} notes, this term is problematic and may be misleading because it does not cover the actual properties of the phenomenon (e.g.\ not all alternations covered by it are in fact assimilations).
She offers the term \emph{consonant mutation} as a more apt one; I adopt this term here because it is more accurate and benefits from being used as a general linguistic term.%
\footnote{%
	The fact \textcite{yang.ch-y.h:2015:mutations-fuzhou} deals with another language (Fúzhōu, an Eastern Mǐn language) is not relevant: the consonant mutation systems in Fúzhōu and Púxiān share many properties in common and can (and should) share a term to describe them.
}
The consonant mutations in Mǐn languages are a type of morphophonological alternation of the onset of the second element in high-juncture phrases consisting of two elements (primarily compounds); the nature of the alternation depends on the rhyme%
\footnote{In Chinese linguistics a \emph{rhyme} refers to the nucleus of a syllable~+ an optional coda.}
of the first element and the onset of the second one \parencites{chen.h:2018:assimilation-xianyou}[§~1.4.1]{wu.j:2010:pronominal-puxian}.

\Cref{fig:xianyou-mutations} is based on table~5 in \textcite{chen.h:2018:assimilation-xianyou} and gives an overview of the consonant mutations in Xiānyóu.

\begin{table}
	\label{fig:xianyou-mutations}%
	\caption{%
		Reproduction of table~5 in \textcite{chen.h:2018:assimilation-xianyou} with stylistic changes for clarity.
		\grapheme{\_} designates the mutated consonant;
		\grapheme{C} an initial consonant (of the previous word);
		\grapheme{V} a non-nasal vowel, diphthong or triphthong;
		\grapheme{Ṽ} a nasal vowel;
		\grapheme{N} a nasal consonant;
		\grapheme{R} a rhyme;
		\grapheme{∅} a zero initial.
		An empty cell designates no alternation.
		The realization of the glottal stop coda (\ipaem{-ʔ}) in column~\emph{a} depends on the following consonant (\ipaet{p}, \ipaet{t} or \ipaet{k} followed by a bilabial, alveolar or velar consonant, respectively), resulting in gemination with plosives.
		Emphasis mine.
	}
	\begin{tabular}{llccccc}
		\toprule
		& & a & b & c & d & e\\
		& & (C)Vʔ.\_ & (C)V.\_V & (C)Ṽ.\_V & \Stack{(C)V.\_Ṽ or\\(C)Ṽ.\_Ṽ} & (C)(V)N.\_R\\
		\midrule
		1 & p, pʰ                                                     &  & β          & β\textasciitilde m          & m & m\\
		2 & t, tʰ, {\boldll}, ts, tsʰ &  & \textbf{l} & \textbf{l\textasciitilde n} & n & n\\
		3 & k, kʰ, h, ʔ                                               &  & ∅          & ∅                           & ∅ & ŋ\\
		4 & m, n, l, ŋ       \\
		\bottomrule
	\end{tabular}
\end{table}

Before discussing the relevance of the Xiānyóu consonant mutations to this study, let us clarify by providing some examples from \textcite{chen.h:2018:assimilation-xianyou}, demonstrating various mutations in \cref{tab:xianyou-mutations-examples}.

\begin{table*}
	\label{tab:xianyou-mutations-examples}%
	\caption[][1\baselineskip]{% % Ugly!!!
		Examples for Xiānyóu mutations in compounds.
		Emphasis mine.
	}
	\begin{tabular}{l>{/}l<{/} >{‘}l<{’} c >{/}l<{/} >{‘}l<{’} c >{[}l<{]} >{‘}l<{’}}
		\toprule
		mutation &
		\multicolumn{2}{l}{first element} & &
		\multicolumn{2}{l}{second element} & &
		\multicolumn{2}{l}{compound}\\
		\midrule
		1a & piʔ²⁴   & honey     &  & pʰaŋ⁵⁵ & bee      &  & pip³⁵.pʰaŋ⁵⁴ & honeybee\\
		1b & tsau⁵²  & fireplace &  & piŋ⁵⁵  & edge     &  & tsau⁴⁴.βiŋ⁵⁴ & the edge of the fireplace\\
		2b & tsʰui⁵² & mouth     &  & tɔ⁵⁵   & dry      &  & tsʰui⁴⁴.lɔ⁵⁴ & thirsty\\
		2b & tua¹¹   & big       &  & {\boldll}\textbf{ya}²⁴  & snake    &  & tua²⁴.\textbf{lya}²⁴  & (big?) snake\\
		3e & aŋ²⁴    & red       &  & kou⁵⁵  & mushroom &  & aŋ²⁴.ŋõũ⁵⁴   & red mushroom\\
		4b & hi¹¹    & ear       &  & laŋ²⁴  & deaf     &  & çi²⁴.laŋ²⁴   & deaf\\
		\bottomrule
	\end{tabular}
\end{table*}

While there are evident differences between the consonant mutation systems in Welsh and in Xiānyóu (both in the actual phonological alternations~— that operate in markedly different phonological systems~— and in their use, syntax and function%
\footnote{
	One fundamental difference is the fact that the Welsh mutations are not dependent on (morpho-)phonological contact but are a part of larger syntactic structures, while the Xiānyóu seem to be dependent on phonological contact (which is much more common in the world’s languages).
	The Welsh soft mutation, for example, serves as a copular link in the \emph{i- cum infinitivo} nexal pattern (a \emph{that}-clause), as discussed by \textcite[§~4.a.2]{shisha-halevy.a:2003:juncture-welsh}; for example:
	\foreign{Y pity yw fod rhaid \colhl{blue}{i} \colhl{cyan}{drychineb} \colhl{green}{ddigwydd} cyn \colhl{blue}{i}\colhl{cyan}{nni} \colhl{green}{ddysgu}’r gwirionedd}
	‘The pity is that it is necessary \colhl{blue}{for} \colhl{cyan}{a disaster} \colhl{green}{to happen} before we learn (lit.\ before \colhl{blue}{for} \colhl{cyan}{us} \colhl{green}{to learn}) the truth’
	(\foreign{ddigwydd} \ipaem{ð-} and \foreign{ddysgu} \ipaem{ð-} are the soft-mutated forms of \foreign{digwydd} \ipaem{d-} ‘to happen’ and \foreign{dysgu} \ipaem{d-} ‘to learn’, the soft mutation being required as a part of the \emph{i- cum infinitivo} pattern).
}),
there are several striking similarities:
column~\emph{b} in \cref{fig:xianyou-mutations} is roughly analogous to the Welsh soft mutation,
column~\emph{e} to the nasal mutation,
columns~\emph{c} and~\emph{d} combine properties of columns~\emph{b} (‘soft mutation’) and~\emph{e} (‘nasal mutation’),
and column~\emph{a} seems to resemble the historical predecessor of the modern aspirate mutation%
\footnote{%
	The fricativization of \ipaem{p}, \ipaem{t} and \ipaem{k} is a development of earlier \reconstructed{pp}, \reconstructed{tt} and \reconstructed{kk}, which is not limited to mutations; see \textcite[§§~145–147, 183–185]{jackson.k:1953:language-history}.
}.
The origin of the soft mutation is intervocalic lenition \parencite[§§~131–143]{jackson.k:1953:language-history}, like in column~\emph{b}, and that of the nasal mutation is assimilation to a preceding nasal consonant \parencite[§§~186–189]{jackson.k:1953:language-history}, like in column~\emph{e}.

The point of likening the two systems is to demonstrate similar treatment of \ipaem{ɬ} in somewhat similar morphophonological systems:
not only both languages have an \ipaem{ɬ} phoneme, and not only both languages have a consonant mutation system, both mutate \ipaem{ɬ} to \ipaem{l}.
While in Welsh the connection between \ipaem{ɬ} and \ipaem{l} is both synchronic and diachronic, Xiānyóu shows similar synchronic connection while its \ipaem{ɬ} has no diachronic connection to \ipaem{l} (as \ipaem{ɬ} developed from sibilant fricatives).%
\footnote{%
	One phonotactic corollary of the change of word-initial \reconstructed{l-}~>~\ipaem{ɬ-} in Welsh is the very limited number of words in Modern Welsh that begin with an \ipaem{l-}, most of them post-change loanwords such as \foreign{lôn} \ipaem{loːn} ‘lane’ and \foreign{lafant} \ipaem{laˈvant} ‘lavender’.
	On the other hand, phonemic \opposition{\ipaem{l-}}{\ipaem{ɬ-}} oppositions are prevalent in Xiānyóu (e.g.\ \Chinese{時} \ipaet{ɬi²⁴} ‘time’ versus \Chinese{梨} \ipaet{li²⁴} ‘pear’).
}

Consonant mutations in Sinitic languages are not limited to Púxiān.
This phenomenon is found in other, Eastern, Mǐn languages such as Fúzhōu \parencite[§~2.2]{yang.ch-y.h:2015:mutations-fuzhou}.%
\footnote{%
	\textcite[§~6]{chen.h:2018:assimilation-xianyou} reports that some scholars, such as \textcite{li.r+:2008:minnan-fangyan}, hold the view that Púxiān was influenced by Eastern Mǐn dialects.
	\textcite[21]{wu.j:2010:pronominal-puxian} states consonant mutation is a phenomenon common to Mǐn (in general?).
}
The Fúzhōu analogue to column~\emph{b} in \cref{fig:xianyou-mutations} is given in \textcite[(6), p.~5]{yang.ch-y.h:2015:mutations-fuzhou}: the relevant fact for our interest is that \ipaem{t-}, \ipaem{tʰ-} and \ipaem{s-} are mutated to \ipaet{l-}.
As stated above, Púxiān \ipaem{ɬ-} derives from earlier sibilant fricatives; here the Fúzhōu sibilant fricative \ipaem{s-} (a cognate of Púxiān \ipaem{ɬ-}) shows a similar synchronic behaviour to Púxiān \ipaem{ɬ-}.

Another phenomenon concerning lateral fricatives in Sinitic languages is that of lateralization.
It is quite limited, and occurs in Yìyáng (from the New Xiāng group), as demonstrated in \cref{tab:yiyang-lateralization}.
\textcite[55–56]{bu.t:2018:regularities-irregularities} discusses the explanation given by \textcite{xia.l:2008:obstruent-yiyang} for the change: an intermediate \ipaem{ɮ-} stage between the (post-)alveolar consonants and \ipaem{l-}: at first the stops merged into their corresponding fricatives, then the (post-)alveolar fricatives all merged into \ipaem{ɮ-}, and finally \ipaem{ɮ-}~>~\ipaem{l-}.
This does not explain Middle Chinese \Chinese{爬} \foreign{bɯa}~>~Yìyáng \foreign{la}, though.

\begin{table}
	\label{tab:yiyang-lateralization}%
	\caption[][0\baselineskip]{% % Ugly!!!
		Lateralization in Yìyáng;
		reproduced from \textcite[table~8, p.~55]{bu.t:2018:regularities-irregularities}.
		MC stands for Middle Chinese.
	}%
\begin{tabular}{cccccccccc}
	\toprule
	& \Chinese{長} & \Chinese{常} & \Chinese{柴} & \Chinese{賤} & \Chinese{乘} & \Chinese{尋} & \Chinese{茶} & \Chinese{蛇} & \Chinese{爬}\\
	\midrule
	MC &
	ɖiɐŋ & dʑiɐŋ & dʐɯæ & dziɛn & ʑɨŋ & zim & ɖɯa & ʑia & bɯa\\
	Yìyáng &
	lɔ̃ & lɔ̃ & lai & liẽ & lən & lin & la & la & la\\
	\bottomrule
\end{tabular}
\end{table}
