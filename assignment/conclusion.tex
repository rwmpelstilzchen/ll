\section{Conclusion}

This paper presented some very preliminary findings concerning aspects of lateral fricatives in the world’s languages.
It touched upon quantitative, structural and geographical phonological typology, phonological change and diachronic pathways, morphophonology, phonotactics, loan-word phonology, language contact, and typological universals and rara.
Much is yet to be done in order to gain a better understanding of these phonemes, and I hope this paper contributes towards that.



\subsection{Venues for further research}
\label{sec:further-research}

One way our understanding can improve is to analyze aggregated information from databases.
Such databases provide a lower resolution but wider typological view in comparison to case studies, which makes the lion’s share of the paper in its current form; both ways complement each other.
Among the relevant databases are
BDPROTO (\cite{marsico.e+:2018:bdproto}; phonological inventories from ancient and reconstructed languages),
PHOIBLE (\cite{moran.s+:2019:phoible-2.0}; PHOIBLE has been used in this paper but can yield much more information),
LAPSyD (\cite{maddieson.i+:2013:lapsyd}; phonological systems),
P-base (\cite{mielke.j:2008:distintive}; phonological rules),
the World Phonotactics Database \parencite{donohue.m+:2013:phonotactics},
the Database of Eurasian Phonological Inventories \parencite{nikolaev.d+:2015:eurasian}
and SegBo (an in-progress survey of phonological segment borrowing, by Grossman, Nikolaev, Moran et al.).

My hope is to broaden the scope of the paper for a future, published version in three ways:
\begin{compactitem}
	\item employing the said databases in expanding and delving deeper into the wide-scale typology of lateral fricatives,
	\item examining more case studies in order to observe the similarities and differences in the roles lateral fricatives play in the diachrony and synchrony of different languages,
	\item discussing topics which were not discussed at all or merely touched upon in the current version.
\end{compactitem}

Such topics include the following:

\begin{description}
	\item [The voiced lateral fricative.]
		 Only a single case study of \ipaem{ɮ} is discussed in the current version (Mongolian, \cref{sec:mongolian}).
	\item [The lateral affricates.]
		As shown in \cref{fig:phoible-map} (bottom maps) and stated above, the geographic distribution of these phonemes is mainly limited to two geographical areas: Tanzania and western North America.
		This fact has to be taken into account in any typological or historical discussion of the lateral affricates.
	\item [The internal relationship between lateral fricatives~/ affricates.]
		Many languages have several lateral fricative or affricate phonemes; do they show morphophonological or other interrelations?
	\item [Phonological (and orthographic) subsitutions.]
		Imperfect imitations of phonological segments occur in two main scenarios: when speakers of other languages, with different phonological inventories, encounter a foreign language and during the period of first language acquisition in children (see \cite{ball.m.j+:2001:acquisition-ll} for Welsh and \cite{mowrer.d.e+:1991:acquisition-xhosa} for Xhosa, two languages with lateral fricatives).
		The renditions of the acoustically distinctive%
		\footnote{%
			Speakers of languages with lateral fricatives seem to be aware of the uniqueness of these sounds, at least in areas where these sounds are uncommon.
			For example, with regards to the uniqueness of sound of Arabic \foreign{ḍ} in the linguistic landscape of Arabic, the language itself was traditionally called \Arabic{لغة الضاد} \foreign{luġat aḍ-ḍād} ‘the language of the \foreign{ḍād}’ \parencite[121]{versteegh.k+:2014:arabic}.
			Similarly, in \textcite{tench.p:2012:sounds-languages}, when working with a group of men who spoke Tera (Chadic; Nigeriaw), the author writes he was ‘astonished to discover Welsh “ll”s in their language’ and had ‘no expectation of hearing a distinctively Welsh sound out there’.
		}
		lateral fricatives~— which many foreigners find difficult to pronounce and children master on a relatively late age \parencite{ball.m.j+:2001:acquisition-ll}~— are relevant for synchronic-typological linguistic description of these phonemes, as well as diachronic one (because of intergenerational transmission, as discussed above).
		Spanish \foreign{alcalde}, Malay \foreign{dloha}, Hausa \foreign{haila̱}, Somali \foreign{árli} and Greek \Greek{βάλσαμον} \foreign{bálsamon}, \Greek{χαλδαίοι} \foreign{kʰaldaíoi} and \Greek{μάτλια} \foreign{mátlia} (\cref{sec:semitic}) are some examples.
		Welsh surnames, first names and place names provide many examples for imperfect renditions of \ipaem{ɬ} by English speakers.
		For example, \textcite{morgan.t.j+:1985:welsh-surnames} lists under the common Welsh surname \foreign{Llwyd} (from Welsh \foreign{llwyd} \ipaem{ɬʊɨd} ‘grey, brown, faint, wan, muddy (of water)’) dozens of spelling variations, including \foreign{Floyd}, \foreign{Flewitt}, \foreign{Luyd} and \foreign{Thloyd}.
		Most variations seem to begin with \foreign{Fl-}, thus splitting the features of \ipaem{ɬ} into sequential fricative and lateral components; cf.\ the anglicized Muscogee Creek name \foreign{Thlopthlocco}
		(from Creek \foreign{Rvp-Rakko}; \grapheme{r} designates \ipaem{ɬ} in the traditional orthography, see \textcite[§~9]{martin.j.b+:2011:grammar-creek}).
		See \textcite[124–126]{steiner.r:1977:laterals-semitic} for references to cases of such renditions of lateral fricatives in Modern South Arabian languages, Welsh, Adygian, Avar and Zulu by foreigners.
		%The anglicized Welsh names \foreign{Floyd} \ipaem{flɔɪd} (from Welsh \foreign{Llwyd} \ipaem{ɬʊɨd}) and \foreign Fluellen (from Welsh \foreign{Llywelyn} \ipaem{ɬəˈwɛlɪn}; see \cite[Wales, p.~535]{innes.p:2007:class-society}) or Creek name \foreign{Thlopthlocco} (from Creek \foreign{Rvp-Rakko}; \grapheme{r} designates \ipaem{ɬ} in the traditional orthography, see \cite[§~9]{martin.j.b+:2011:grammar-creek}) demonstrate splitting the features of \ipaem{ɬ} into a fricative followed by an \ipaem{l}; \foreign{Lloyd}
	\item [Orthography.]
		On a similar note: how are the phonemes in question written in the orthographies of languages that have them in their inventories, either as proper phonemes or as allophones of other phonemes.
		This question is ‘para-phonological’, but it can offer a glimpse into the way people perceive these sounds:
		either people from within the speech community or from without, such as linguists who devise writing system for the use of communities speaking languages with no writing tradition.
		\textcite[n.~1 on p.~10 and n.~5 on p.~11]{steiner.r:1977:laterals-semitic} lists some symbols for \ipaem{ɬ} and \ipaem{ɮ}.
		Although not academic \emph{per se} the respective Wikipedia pages lists many examples in orthography and IPA transcription.
		The history of the IPA symbols \grapheme{\ipa{ɬ}} and \grapheme{\ipa{ɮ}} is a meta-linguistic question;
		according to \textcite{ipa:1928:decisions-officielles} they became official in 1928, on the basis of published linguistic works, but ten years later \grapheme{\ipa{ɮ}} was replaced by another, graphically similar symbol \grapheme{\raisebox{-0.5ex}{\includegraphics[height=2ex]{lezh-old.pdf}}}, only to be later re-introduced.
		Its shape and official name (the IPA handbook lists it as ‘L-Ezh ligature’ and in Unicode it is called \textsc{latin small letter lezh}) suggest it is a ligature of \grapheme{l} and \grapheme{ʒ} (‘ezh’); is this a case similar to the \foreign{fl} or \foreign{thl} representation discussed above, splitting the features between two components?
		% https://www.phon.ucl.ac.uk/home/wells/blog0611a.htm
	\item [Borrowability and language areas.]
		Examples like the phonological and orthographic substitutions discussed above demonstrate cases in which these sounds resist borrowing; yet the maps in \cref{fig:phoible-map} suggest language areas where they are shared between languages of different genealogical affiliation.
	\item [Dialectal variation.]
		Some languages exhibit dialectal variation with regards to the sounds in question.
		For example, the northern dialects of Norwegian, from Trondheim and northwards, devoice \ipaem{l} after a short vowel and before a voiceless stop; \textcite[n.~21 on p.~79]{kristoffersen.g:2000:phonology-norwegian} describes it as a voiceless lateral fricative (\ipaem{ɬ}), while \textcite[36]{vanvik.a:1979:fonetikk} describes it as a voiceless lateral approximant (\ipaem{l̥}).
		In other dialects of Norwegian it is basically voiced in all positions.
\end{description}

\begin{center}
	{\fontspec{Symbola}⁘}
\end{center}

\noindent
I wish to finish this paper with a quotation from a talk by Welsh poet, playwright and translator Gillian Clarke.%
\footnote{The talk is available on YouTube:\\\url{https://youtu.be/H8tHFuvRJAo?t=219}}
While the theory suggested in it can be easily disproved by a glance at \cref{fig:phoible-map} (top left), this does not diminish whatsoever its poetic quality…

\begin{quotation}
	My favourite sound is the double \foreign{l}, and it’s that [ɬːː] sound, and I’ve got a completely unproved theory that is because three sides of Wales is surrounded by the sea and most of the people live in those three sides, and the sound of the tide breaking on the shore is a kind of [ɬə] sound. My completely ridiculous theory is that this sound developed in the Welsh language (and as far as I know, not in any other language at all) because we’re all \emph{sea people}.
\end{quotation}


%About Sinitic:
%I have very limited knowledge in Sinitic linguistics~— either historical or synchronic~— and the linguistic situation of these languages is truly complex.
%For later versions of this paper I will have to consult with an expert in the field.
%About Fúzhōu:
%I do not know the diachronic details behind this:
%is it a parallel, convergent change?
%Was there a \ipaem{ɬ-}~>~\ipaem{s-} change in Fúzhōu (and other languages?) due to contact with other Sinitic languages with sibilant fricatives?
%I need to learn more about the synchronic and diachronic situation in order to be able to answer these questions.
