\section[Typological overview]{%
	Typological overview%
	\footnote{%
		\textcite[§~6.3]{ladefoged.p+:1996:sounds-languages} give a survey of laterals other than the voiced lateral approximants.
		This typological overview is intended to complement it.
		See also \textcite{maddieson.i:2013:wals-8}.
	}%
}
\label{sec:overview}

The prevalence of specific phonological segments varies greatly: some are extremely common, yet most are vanishingly rare, as can be seen in \cref{fig:phoible-plot}.%
\footnote{%
 	The PHOIBLE data in CSV format~— downloaded from the Git repository in February 20, 2019 (\url{phoible.org/download})~— is used for \cref{fig:phoible-plot,fig:ratio}.
	%Thus, there is a disparity between the data presented here and the PHOIBLE web interface (\url{phoible.org}), which reflects 2014 data.
	%The reason for using the newer data (apart from corrections and addition of new languages) is that it distinguishes between ‘\ipa{ɬ̪}’ \foreign{per se} (voiceless dental lateral fricative that is not alveolar) and ‘\ipa{ɬ̪|ɬ}’ (no clear distinction); a distinction used in \cref{fig:ratio}.
	%For technical reasons the maps use the 2014 data.
}
Both extremities pose difficulties for typological description: the most common ones are so common and occur in such diverse types of phonological systems that one can hardly generalize anything non-trivial about them, and the rare ones provide too little information (about a half of the segments are represented only in a single inventory; this is marked by 1524 on the horizontal axis in the figure).
%To borrow the metaphor used in astrobiology, there are segments which are in a ‘Goldilocks zone’\todo{generalize! \url{https://en.wikipedia.org/wiki/Goldilocks_principle}}%
%\footnote{In astrobiology this term refers to the more precisely-worded notion of circumstellar habitable zone (CHZ), the range of orbits around a star within which a planetary surface can support liquid water given sufficient atmospheric pressure and thus possibly life as we know it: a zone which is not too hot nor too cold.}:
To borrow the metaphor used in other fields, there are however segments which are in a ‘Goldilocks zone’: they are not too rare, yet not too common.
The alveolar lateral fricatives are such segments.

\begin{figure}
	\label{fig:phoible-plot}%
	\caption{%
		Representation of segments in PHOIBLE \parencite{moran.s+:2019:phoible-2.0}.

		Segments are arranged along the blue line.
		The vertical axis represents how common a segment is in terms of the number of inventories in which it is present;
		the horizontal axis represents how many segments are more common than the segment in question.
		For example, \ipaem{ɮ} occurs in 48 inventories and 216 segments are more common than it;
		the most common segment, for comparison, is \ipaem{m}, which occurs in 2522 inventories and has no segments that are more common than it.

		The steep decline shows how a small group of segments is represented in a large portion of the world’s languages, leaving a very long tail.
		The lateral fricatives are located in the higher (leftmost in the figure) part of this tail.
	}%
	\newcommand{\plotpoint}[4]{#1\\[#4]\tiny (#2, #3)}%
	%\resizebox{\textwidth}{!}{
	\begin{center}
		\begin{tikzpicture}
			\begin{axis}[
				tick label style={/pgf/number format/assume math mode=true,font=\addfontfeature{Numbers=Lining}\scriptsize},
				axis line style={draw=none},
				xlabel={\small number of segments which are more common than the segment},
				ylabel={\small number of inventories in which the segment is present},
				xtick={0,97,216,1524,3204},
				ytick={2522,149,48,1},
				xticklabels={0,\smash{\raisebox{1.5em}{97}},216,1524,3204},
				yticklabels={2522,149,48,\smash{\raisebox{-1.5ex}{1}}},
				]
				\addplot [myblue,thick] table [dotted, mark=none, name path=plotA, x=rank, y=count, col sep=comma]
				{/home/me/documents/linguistics/typology/phonology/course/ll/assignment/phoible-phonemes-count.csv};
				\draw [stealth-,thick] (axis cs:0,2522) -- (axis cs:707,2522+0)     node[align=center, right]{\plotpoint{m}{0}{2522}{-2.0ex}} ;
				\draw [stealth-,thick] (axis cs:97,149) -- (axis cs:98+500,149+500) node[align=center, above]{\plotpoint{ɬ}{97}{149}{-2.0ex}} ;
				\draw [stealth-,thick] (axis cs:216,48) -- (axis cs:217+500,48+500) node[align=center, right=-0.5em]{\plotpoint{ɮ}{216}{48}{-1.5ex}} ;
				\draw [stealth-,thick] (axis cs:3204,1) -- (axis cs:3204,1+707)     node[align=center, above]{\plotpoint{ŋ̥ǂ͓ˡxˀ}{3204}{1}{-1.0ex}} ;
			\end{axis}
		\end{tikzpicture}
	\end{center}
	%}
	\setfloatalignment{b}
\end{figure}

In general lateral fricatives are implicationally found in languages with lateral approximants (\textcite{maddieson.i:2013:wals-8} lists 47 languages as ‘\ipaem{l} and lateral obstruent’ but only 8 as ‘no \ipaem{l}, but lateral obstruents’; see also \cite[9]{steiner.r:1977:laterals-semitic}), but this is weakly predictive, as \ipaem{l} is quite widespread to begin with: the total proportion of languages with \ipaem{l} in \textcite{maddieson.i:2013:wals-8} is
$\frac{388+47}{95+388+29+47+8} \approx 0.77$,
while the proportion in languages with lateral obstruents is only slightly higher:
$\frac{47}{47+8} \approx 0.85$.

\Cref{fig:ratio} shows the number of inventories in PHOIBLE that include alveolar or dental fricatives and affricates.
The voiceless fricatives here greatly outnumber the voiced ones, as is the case with most fricatives \parencite[§§~3.3 and 3.4]{maddieson.i:1984:sounds}%
\footnote{%
	\textcite[10]{steiner.r:1977:laterals-semitic} presents another possible contributing factor for the relative rarity of \ipaem{ɮ}: the ubiquitous voiced lateral approximants (of which \ipaem{l} is by far the most common) tend to discourage the formation and/or retention of \ipaem{ɮ}, phoneme which would diminish their margin of safety (in other words, the distinction between them is not salient enough).
}.
The ‘voicing ratio’ in PHOIBLE is somewhat higher than in Maddieson’s quota sample (UPSID, see \cite[§~10.2]{maddieson.i:1984:sounds}): $\frac{48+3+2}{149+24+9} \approx 0.29$ rather than $\frac{5+2+0}{13+17+0} \approx 0.23$ \parencite[§~3.3]{maddieson.i:1984:sounds}.
The voiceless affricates are about half as common as the voiceless fricatives, and the voiced ones show a much lower ratio of about a fifth in comparison to the voiced fricatives; this difference is due to the prevalence of voiceless lateral affricates in the languages of North America (see \cref{fig:phoible-map}).
In addition, see \textcite[78]{maddieson.i:1984:sounds} regarding the distribution of various features of lateral segments.

\begin{figure*}
	\label{fig:ratio}%
	\setstretch{0.75}%
	\newcommand{\lefttitlewidth}{\baselineskip}%
	\newcommand{\totalratblock}{182}%
	\begin{minipage}{\lefttitlewidth}%
		\rotatebox{90}{voiceless}
	\end{minipage}%
	\begin{minipage}{\textwidth - \lefttitlewidth}%
		\colratblock{\ipa{ɬ}}{149}{}{\totalratblock}{sronpaleblue}%
		\colratblock{\ipa{ɬ̪|ɬ}}{24}{}{\totalratblock}{sronpaleblue}%
		\colratblock{\ipa{ɬ̪}}{9}{}{\totalratblock}{sronpaleblue}

		\colratblock{\ipa{tɬʼ}}{28}{}{\totalratblock}{sronpalecyan}%
		\colratblock{\ipa{tɬ}}{15}{}{\totalratblock}{sronpalecyan}%
		\colratblock{\ipa{\scriptsize t̪ɬ̪ʼ|tɬʼ}}{11}{}{\totalratblock}{sronpalecyan}%
		\colratblock{\ipa{tɬʰ}}{11}{}{\totalratblock}{sronpalecyan}%
		\colratblock{\tiny\ipa{t̪ɬ̪|tɬ}}{6}{}{\totalratblock}{sronpalecyan}%
		\colratblock{\tiny\ipa{tɬːʼ}}{4}{}{\totalratblock}{sronpalecyan}%
		\colratblock{}{3}{\leavevmode\phantom}{\totalratblock}{sronpalecyan}%t̪ɬ̪ʰ|tɬʰ
		\colratblock{}{3}{\leavevmode\phantom}{\totalratblock}{sronpalecyan}%tɬːʰ
		\colratblock{}{2}{\leavevmode\phantom}{\totalratblock}{sronpalecyan}%t̪ɬ̪ʰ
		\colratblock{}{2}{\leavevmode\phantom}{\totalratblock}{sronpalecyan}%tɬː
		\colratblock{}{2}{\leavevmode\phantom}{\totalratblock}{sronpalecyan}%t̪ɬ̪
		\colratblock{}{1}{\leavevmode\phantom}{\totalratblock}{sronpalecyan}%ˀt̪ɬ
		\colratblock{}{1}{\leavevmode\phantom}{\totalratblock}{sronpalecyan}%t̪ɬ̪ʼ
		\colratblock{}{1}{\leavevmode\phantom}{\totalratblock}{sronpalecyan}%t̪ɬʼ
		\colratblock{}{1}{\leavevmode\phantom}{\totalratblock}{sronpalecyan}%t̪ɬ̪ˀ|tɬˀ
		\colratblock{}{1}{\leavevmode\phantom}{\totalratblock}{sronpalecyan}%t̪ɬ̪ʷʼ|tɬʷʼ
		\colratblock{}{1}{\leavevmode\phantom}{\totalratblock}{sronpalecyan}%tɬʷʼ
		\colratblock{}{1}{\leavevmode\phantom}{\totalratblock}{sronpalecyan}%t̪ɬ̪ʰʷ|tɬʰʷ
		\colratblock{}{1}{\leavevmode\phantom}{\totalratblock}{sronpalecyan}%tɬʰʷ
		\colratblock{}{1}{\leavevmode\phantom}{\totalratblock}{sronpalecyan}%t̪ɬ̪ːʼ|tɬːʼ
		\colratblock{}{1}{\leavevmode\phantom}{\totalratblock}{sronpalecyan}%t̪ɬ̪ː|tɬː
		\colratblock{}{1}{\leavevmode\phantom}{\totalratblock}{sronpalecyan}%t̪ɬ
		\colratblock{}{1}{\leavevmode\phantom}{\totalratblock}{sronpalecyan}%tɬ̰
		\quad
		(24 occurrences not labeled here)

		\colratblock{\ipa{ɬʼ}}{8}{}{\totalratblock}{sronpalegreen}%
		\colratblock{\ipa{ɬʲ}}{8}{}{\totalratblock}{sronpalegreen}%
		\colratblock{\ipa{ɬː}}{6}{}{\totalratblock}{sronpalegreen}%
		\colratblock{\ipa{}}{2}{\leavevmode\phantom}{\totalratblock}{sronpalegreen}%ɬ̪ʼ|ɬʼ
		\colratblock{\ipa{}}{2}{\leavevmode\phantom}{\totalratblock}{sronpalegreen}%ɬʷ
		\colratblock{\ipa{}}{2}{\leavevmode\phantom}{\totalratblock}{sronpalegreen}%ɬˠ
		\colratblock{\ipa{}}{2}{\leavevmode\phantom}{\totalratblock}{sronpalegreen}%ɬ̪ː|ɬː
		\colratblock{\ipa{}}{1}{\leavevmode\phantom}{\totalratblock}{sronpalegreen}%ɬ̪ʷ|ɬʷ
		\colratblock{\ipa{}}{1}{\leavevmode\phantom}{\totalratblock}{sronpalegreen}%ɬ̪ʲʼ
		\colratblock{\ipa{}}{1}{\leavevmode\phantom}{\totalratblock}{sronpalegreen}%ɬʲʼ
		\colratblock{\ipa{}}{1}{\leavevmode\phantom}{\totalratblock}{sronpalegreen}%ɬ̪ʲ|ɬʲ
		\colratblock{\ipa{}}{1}{\leavevmode\phantom}{\totalratblock}{sronpalegreen}%ɬ̪ʲ
		\colratblock{\ipa{}}{1}{\leavevmode\phantom}{\totalratblock}{sronpalegreen}%ɬ̪ːʷ|ɬːʷ
		\colratblock{\ipa{}}{1}{\leavevmode\phantom}{\totalratblock}{sronpalegreen}%ɬ̪ː
		\colratblock{\ipa{}}{1}{\leavevmode\phantom}{\totalratblock}{sronpalegreen}%ɬ̪̺
		\colratblock{\ipa{}}{1}{\leavevmode\phantom}{\totalratblock}{sronpalegreen}%ɬ̻
		\quad
		(17 occurrences not labeled here)

		\colratblock{\ipa{}}{1}{\leavevmode\phantom}{\totalratblock}{sronpalered}%
		\colratblock{\ipa{}}{1}{\leavevmode\phantom}{\totalratblock}{sronpalered}\quad
		(\ipaem{nɬ} and \ipaem{kɬ}, 1 occurrence each)
	\end{minipage}

	\vspace{\baselineskip}

	\begin{minipage}{\lefttitlewidth}
		\rotatebox{90}{voiced}
	\end{minipage}
	\begin{minipage}{\textwidth - \lefttitlewidth}
		\colratblock{\ipa{ɮ}}{48}{}{\totalratblock}{sronpaleblue}%
		\colratblock{}{3}{\leavevmode\phantom}{\totalratblock}{sronpaleblue}%
		\colratblock{}{2}{\leavevmode\phantom}{\totalratblock}{sronpaleblue}\quad
		{(\ipaem {ɮ̪|ɮ} and \ipaem{ɮ̪}, 3 and 2 respectively)}

		\colratblock{\tiny\ipa{d̪ɮ̪|dɮ}}{5}{}{\totalratblock}{sronpalecyan}%
		\colratblock{\tiny\ipa{dɮ}}{4}{}{\totalratblock}{sronpalecyan}%
		\colratblock{}{1}{\leavevmode\phantom}{\totalratblock}{sronpalecyan}\quad
		(\ipaem{d̤ɮ̤}, 1 occurrence)

		\colratblock{\small\ipa{ɮʲ}}{4}{}{\totalratblock}{sronpalegreen}%
		\colratblock{}{1}{\leavevmode\phantom}{\totalratblock}{sronpalegreen}%
		\colratblock{}{1}{\leavevmode\phantom}{\totalratblock}{sronpalegreen}%
		\colratblock{}{1}{\leavevmode\phantom}{\totalratblock}{sronpalegreen}%
		\colratblock{}{1}{\leavevmode\phantom}{\totalratblock}{sronpalegreen}\quad
		(\ipaem{ɮʼ}, \ipaem{ɮ̪ʲ} \ipaem{ɮ̤} \ipaem{ɮ̺}, 1 occurrence each)

		\colratblock{}{2}{\leavevmode\phantom}{\totalratblock}{sronpalered}%
		\colratblock{}{1}{\leavevmode\phantom}{\totalratblock}{sronpalered}\quad
		(\ipaem{n̤d̤ɮ̤} and \ipaem{nɮ}, 2 and 1 occurrences respectively)
	\end{minipage}

	\let\totalratblock=\undefined

	\caption[][-9.5\baselineskip]{% % Ugly, but \heightof doesn’t seem to work here :-(
		Representation of dental/alveolar fricatives and affricates in PHOIBLE in absolute numbers and internal ratio (bar width is relative to the number).
		The first row (blue,~\colouredlegend{sronpaleblue}) has plain fricatives,
		the second (cyan,~\colouredlegend{sronpalecyan}) affricates,
		the third (green,~\colouredlegend{sronpalegreen}) fricatives with various secondary articulations and types of phonation and
		the fourth (red,~\colouredlegend{sronpalered}) other, rare, complex phonemes (prenasalized or with a preceding \ipaem{k}).
	}
\end{figure*}

I count dental lateral fricatives here together with alveolar ones because the distinction between them is very rare.
In fact, the only case of phonemic \opposition{\ipaet{ɬ}}{\ipaet{ɬ̪}} opposition I am aware of is in Mapuche (Araucanian; Chile, Argentina), where \ipaem{l̪} and \ipaem{l} are phonemic and are realized as their fricative allophones \ipaet{ɬ̪} and \ipaet{ɬ} in utterance-final position \parencite{sadowsky.s+:2013:mapudungun}; for example:

\begin{tabular}{lll}
	\ipaem{kɐ̝ˈɣɘl̪} & \ipaet{kɜˈɣɘɬ̪} & ‘phlegm that is spit’\\
	\ipaem{kɐ̝ˈɘl}  & \ipaet{kɜˈɘɬ}  & ‘a different song’
\end{tabular}

\newcommand{\mapmargin}{0.5em}

The geographical distribution of the lateral fricatives (top) and affricates (bottom) is presented in \cref{fig:phoible-map}; voiceless segments are given on the left and voiced ones on the right.%
\footnote{%
	For technical reasons I had to choose only one phoneme per map.
	I chose the most common representative in the database for each group (\ipaem{ɬ}, \ipaem{ɮ}, \ipaem{tɬʼ}, \ipaem{d̪ɮ̪|dɮ}).
	For future versions of this paper I will learn how to plot locations on a map using the raw data so I can overlay information.
}
\ipaem{ɬ} is the most widespread lateral fricative/affricate, and is found in varying densities in all the continents except perhaps Australia.

\begin{figure}[h]%
	\label{fig:phoible-map}%
	\caption{%
		The geographical distribution of the lateral fricatives
		\ipaem{ɬ} (voiceless, top left),
		\ipaem{ɮ} (voiced, top right),
		\ipaem{tɬʼ} (voiceless, bottom left) and
		\ipaem{d̪ɮ̪|dɮ} (voiced, bottom right) in PHOIBLE.

	}
	\includegraphics[width=0.5\linewidth - \mapmargin]{phoible2-ɬ.png}\hfill
	\includegraphics[width=0.5\linewidth - \mapmargin]{phoible2-ɮ.png}

	\vspace{1em}

	\includegraphics[width=0.5\linewidth - \mapmargin]{phoible2-tɬʼ.png}\hfill
	\includegraphics[width=0.5\linewidth - \mapmargin]{phoible2-d̪ɮ̪|dɮ.png}
\end{figure}

The maps can demonstrate how, broadly speaking, languages with voiced fricatives/affricates make a subset of languages with their voiceless counterparts; this agrees with the general principle given in \textcite[§~7.6.3.ix]{lass.r:1984:phonology}:

\begin{quotation}
	The number of voiceless fricatives is likely to be greater than that of voiced; and there is likely to be an implicational relation between a voiced fricative and its voiceless cognate. The second statement is more weakly predictive than the first, and truer for fricatives than for stops%
	\footnote{\Textcite[§~7.6.1]{lass.r:1984:phonology} includes affricates under the wider term of \emph{stops}.}.
\end{quotation}

Thus, for the most part \ipaem{ɮ} is implicationally found where \ipaem{ɬ} is: eastern Nigeria and northern Cameroun in green circles and Bantu languages to the south-east of Africa in red ones, with Ahtena (Northern Athabaskan, Na-Dené; Alaska) being the sole American language with a voiced lateral fricative in the database (dark reddish-brown), while voiceless lateral fricatives are very prevalent in this continent.

Similarly, voiced lateral affricates are found mainly in Tanzania and western North America, where voiceless lateral affricates are also found.%
\footnote{This has to be refined by overlaying maps.}
See \textcite{maddieson.i:2013:wals-8}:

\begin{quotation}
	If a language has a lateral affricate in its consonant inventory, then this generally entails the presence of a lateral fricative […] this observation makes a meaningful prediction, since 88\% of the languages with affricates also have fricatives.
\end{quotation}

Comparing the distribution of \ipaem{ɬ} (top left) with that of \ipaem{tɬʼ} (bottom left) demonstrates how voiceless lateral fricatives are found all across the Americas, yet the voiceless lateral affricates are an areal North American phenomenon.

Lateral fricatives which are pronounced further to the back are much rarer, found in a few languages each:%
\footnote{Due to their rarity, most have no official, non-compound symbol in IPA; \grapheme{\ipa{ꞎ\,}}, \grapheme{\ipa{l\kern-0.16em ᶚ}}, \grapheme{\ipa{}} and \grapheme{\ipa{}} are non-official, extended symbols.}
the retroflex \ipaem{ꞎ\,} (\ipaem{ɭ̊˔}) and \ipaem{l\kern-0.16em ᶚ} (\ipaem{ɭ˔}),
the palatal \ipaem{} (\ipaem{ʎ̝̊}) and \ipaem{ʎ̝}, and
the velar \ipaem{} (\ipaem{ʟ̝̊}) and \ipaem{ʟ̝}.
On the whole, they are found in languages that have alveolar lateral fricative(s), such as
the retroflex \ipaem{ꞎ\,} in Toda (a Dravidian language from Tamiḻ Nāḍu, India, which also has an alveolar \ipaem{ɬ}; \cites{shalev.m+:1994:phonetics-toda}{spajic.s+:1994:rhotics-toda}),
the velar \ipaem{} in Wahgi (a Chimbu–Wahgi language from New Guinea which also has a dental \ipaem{ɬ̪(ʲ)}; \cite[§~1.1.2.9]{phillips.d.j:1976:wahgi})
or the three-way \ipaem{ɬ}, \ipaem{ɮ} and \ipaem{ʎ̝̊} in Bura (a Chadic language from Nigeria totalling five laterals with its two approximants, \ipaem{l} and \ipaem{ʎ}; \cite[154–155]{gronnum.n:2005:fonetik-fonologi}).
This implicational universal is not absolute: Archi (Northeast Caucasian; Archib, Dagestan) has five (!) palato-velar lateral fricatives (with distinctions of voicing, length and labialization) and four more \ipaem{k-} palato-velar lateral affricates, but no alveolar or dental lateral fricatives \parencite{chumakina.m+:2008:archi-tutorial}.

To the best of my knowledge, the voiceless alveolar lateral approximant \ipaet{l̥} is not reported to contrast in any language with the voiceless alveolar lateral fricative \ipaet{ɬ}.
Although they are phonetically distinct and should be distinguished in description \parencite{maddieson.i+:1984:voiceless-lateral}, the difference between them seems not to be salient enough for languages to phonologize an \opposition{\ipaem{l̥}}{\ipaem{ɬ}} distinction.
Phonetically, \ipaet{l̥} is less intense, has less noise in the higher end of the spectrum and has longer voicing in the later stages of its articulation \parencites[186–187]{maddieson.i+:1984:voiceless-lateral}[198]{ladefoged.p+:1996:sounds-languages}.
\textcite{asu.e.l+:2015:voiceless-laterals} report that their experimental data demonstrate a range of variance between the discrete \opposition{approximant}{fricative} categories; while this calls for refining linguistic descriptions, it does not contradict the findings or conclusions of \textcite{maddieson.i+:1984:voiceless-lateral}, who describe the differences in terms of degree, not as completely categorical divisions.
